var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: './src/main.js',
  output: {
  	path: path.join(__dirname, './dist/'),
  	publicPath: '/dist/',
  	filename: 'bundle.js'
  }, 
  module: {
    rules: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
          test: /\.scss$/,
          use: [{
              loader: "style-loader" // creates style nodes from JS strings
          }, {
              loader: "css-loader" // translates CSS into CommonJS
          }, {
              loader: "sass-loader" // compiles Sass to CSS
          }]
      },
      {
        test: /\.svg$/,
        use: [{
            loader: 'svg-sprite-loader'
        }]
      }
    ]
  }
};


  // loaders: [{
  //     test: /\.svg$/,
  //     loader: 'icons-loader',
  // }],
  // plugins: [
  //   new IconsPlugin({
  //     fontName: 'icons',
  //     timestamp: RUN_TIMESTAMP,
  //     normalize: true,
  //     formats: ['ttf', 'eot', 'woff', 'svg']
  //   })
  // ]
