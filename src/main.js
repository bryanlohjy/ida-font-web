var React = require('react');
var ReactDOM = require('react-dom');
var createReactClass = require('create-react-class');
var AppContainer = require('./js/App.jsx').AppContainer;
var styles = require('./css/styles.scss');
var sprites = require('./manifold-svg/ManifoldData.js');
// var menu  = require('./manifold-svg/font-path-0.svg');
var manifoldIds = [];
sprites.data.forEach(function(sprite) {
	manifoldIds.push(sprite.default.id);
});

ReactDOM.render(<AppContainer manifoldData={manifoldIds}/>, document.getElementById('container'));
