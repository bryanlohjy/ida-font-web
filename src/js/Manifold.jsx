var React = require('react');
var ReactDOM = require('react-dom');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');

var Manifold = createReactClass({
	displayName: 'Manifold',
	getInitialState: function() {
		return ({
			x: 0,
			y: 0,
			scale: 1,
			dragging: false
		});
	},
	isFontSelected: function(fontData) {
		var fontSerialised = this.props.serialiseFontCoords(fontData);
		var selectedFontsSerialised = this.props.serialiseMultiArray(this.props.selectedFonts);
		var index = selectedFontsSerialised.indexOf(fontSerialised);
		if (index > -1) {
			return true;
		}
	},
	handleMouseDown: function(e) {
		e.stopPropagation();
		e.preventDefault();
		var prevX = this.state.x;
		var prevY = this.state.y;
		this.mouseOffset = {
			x: e.pageX - prevX,
			y: e.pageY - prevY
		};
		this.setState({
			dragging: true
		});
	},
	handleMouseMove: function(e) {
		e.stopPropagation();
		e.preventDefault();
		if (this.state.dragging) {
			var newX =  e.pageX - this.mouseOffset.x;
			var newY = e.pageY - this.mouseOffset.y;
			// if (newY >= 0) {
			// 	newY = 0;
			// }
			// if (newX >= 0) {
			// 	newX = 0;
			// }
			this.setState({
				x: newX,
				y: newY
			});
		}
	},
	handleMouseUp: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.mouseOffset = {};
		this.setState({
			dragging: false
		});
	},
	isNegative: function(n) {
		return ((n = +n) || 1 / n) < 0;
	},
	handleMouseWheel: function(e) {
		e.stopPropagation();
		e.preventDefault();
		var direction = (this.isNegative(e.deltaX) &&  this.isNegative(e.deltaY) ) ? 'down' : 'up';
		var zoomStep = 0.1;
		var maxZoom = 3;
		var minZoom = 0.5;
		if (direction == 'up') {
			this.state.scale -= zoomStep;
		} else {
			this.state.scale += zoomStep;
		}
		this.state.scale = (this.state.scale > maxZoom) ? maxZoom : this.state.scale;
		this.state.scale = (this.state.scale < minZoom) ? minZoom : this.state.scale;

		this.setState(this.state);
	},
	render: function() {
		var self = this;
		var grid = [];
		this.props.manifoldData.map(function(rowData, rowIndex) {
			rowData.forEach(function(fontData, columnIndex) {
				var selectedFont = self.isFontSelected([columnIndex, rowIndex]);
				var key = columnIndex + (rowData.length * rowIndex);
				var endRow = (rowIndex == self.props.manifoldData.length - 1) ? true : false;
				var endCol = (columnIndex == rowData.length - 1) ? true : false;
				var maxSelected = (self.props.selectedFonts.length == self.props.maxSelectedFonts) ? true : false; 
				grid.push(<ManifoldItem
							key={key}
							x={columnIndex}
							y={rowIndex}
							selectedFont={selectedFont}
							updateSelectedFonts={self.props.updateSelectedFonts}
							fontData= {fontData}
							endRow={endRow}
							endCol={endCol}
							maxSelected={maxSelected}
						/>);
			})
		});
		return (
			<div 
				id={'manifold-wrapper'}
				onMouseDown={this.handleMouseDown}
				onMouseUp={this.handleMouseUp}
				onMouseMove={this.handleMouseMove}
				onWheel={this.handleMouseWheel}
			>
				<svg
					id={'manifold'}
					style={{
						left: this.state.x,
						top: this.state.y
					}}
					transform={`scale(${this.state.scale})`}
				>
					<g>
						{grid}
					</g>
				</svg>
			</div>
		);
	}
});
Manifold.proptypes = {
	manifoldData: PropTypes.array,
	maxSelectedFonts: PropTypes.number,
	updateSelectedFonts: PropTypes.func,
	selectedFonts: PropTypes.array,
	serialiseFontCoords: PropTypes.func,
	serialiseMultiArray: PropTypes.func
};

var ManifoldItem = createReactClass({
	displayName: 'ManifoldItem',
	handleClick: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.props.updateSelectedFonts([this.props.x, this.props.y]);
	},
	render: function() {
		var selectedClass = (this.props.selectedFont) ? ' selected': '';
		var itemSpacing = 125;
		var startX = itemSpacing/2;
		var startY = itemSpacing/2;
		var x = startX + this.props.x * itemSpacing;
		var y = startY + this.props.y * itemSpacing;
		var radius = 50;
		var maxClass = (this.props.maxSelected && !this.props.selectedFont) ? ' max-selected' : '';
		var endCol = (this.props.endCol) ?
						(<line
							className={'grid grid-horizontal'}
							x1={x + radius + 2}
							y1={y}
							x2={x + itemSpacing/2 - 2}
							y2={y}
						></line>): '';
		var endRow = (this.props.endRow) ?
						(<line
							className={'grid grid-horizontal'}
							x1={x}
							y1={y + radius + 2}
							x2={x}
							y2={y + itemSpacing/2 - 2}
						></line>): '';
		return (
			<g className={'manifold-item' + selectedClass + maxClass}
				x={x}
				y={y}
			>
				<line
					className={'grid grid-horizontal'}
					x1={x - itemSpacing + radius + 2}
					y1={y}
					x2={x - radius - 2}
					y2={y}
				/>
				<line
					className={'grid grid-vertical'}
					x1={x}
					y1={y - itemSpacing + radius + 2}
					x2={x}
					y2={y - radius - 2}
				/>
				<circle 
					cx={x}
					cy={y}
					r={radius}
					onClick={this.handleClick}
				>
				</circle>
				<svg
					x={x - 112} 
					y={y - 90} 
					style={{
							'overflow': 'visible'
							}}
					preserveAspectRatio={'none'}
				>
					<use 
						href= {'#'+ this.props.fontData}
					/>
				</svg>
				{endCol}
				{endRow}
			</g>
		)
	}
});
ManifoldItem.proptypes = {
	updateSelectedFonts: PropTypes.func,
	selectedFont: PropTypes.bool,
	x: PropTypes.number,
	y: PropTypes.number,
	fontData: PropTypes.array,
	endCol: PropTypes.bool,
	endRow: PropTypes.bool,
	maxSelected: PropTypes.bool
};
module.exports = {Manifold: Manifold}
