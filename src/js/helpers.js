var randomInt = function(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max) + 1;
	return Math.floor(Math.random() * (max - min)) + min;
};

module.exports = {randomInt: randomInt};