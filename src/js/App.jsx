var React = require('react');
var ReactDOM = require('react-dom');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');
var Manifold = require('./Manifold.jsx').Manifold;
var ControlPanel = require('./ControlPanel.jsx').ControlPanel;
var helpers = require('./helpers.js');


var AppContainer = createReactClass({
	displayName: 'AppContainer',
	getInitialState: function() {
		return {
			showControls: true,
			selectedFonts: [],
			manifoldWidth: 16,
			manifoldHeight: 16,
			manifoldData: [],
			maxSelectedFonts: 8,
			showModal: false
		};
	},
	componentWillMount: function() {
		this.formatManifoldData(this.props.manifoldData);
	},
	formatManifoldData: function(manifoldData) {
		var temp = [];
		for (var i in manifoldData) {
			var k = i%this.state.manifoldWidth;
			var font = manifoldData[i];
			if (k == 0) { // new row
				temp.push([font]);
			} else {
				temp[temp.length-1].push(font);
			}
		}
		this.setState({
			manifoldData: temp
		});
	},
	serialiseFontCoords: function(fontCoord) {
		return fontCoord[0].toString() + fontCoord[1].toString();
	},
	serialiseMultiArray: function(multiArray) {
		var temp = [];
		multiArray.forEach(function(arr) {
			temp.push(arr[0].toString() + arr[1].toString());
		});
		return temp;
	},
	isFontSelected: function(fontData, selectedFonts) {
		var fontSerialised = this.serialiseFontCoords(fontData);
		var selectedFontsSerialised = this.serialiseMultiArray(selectedFonts);
		var index = selectedFontsSerialised.indexOf(fontSerialised);	
		return index;
	},
	updateSelectedFonts: function(fontData) {
		var index = this.isFontSelected(fontData, this.state.selectedFonts);
		var temp = this.state.selectedFonts.slice();
		if (index > -1) { // if font is selected, remove
			temp.splice(index, 1);
		} else if (index < 0 && temp.length < this.state.maxSelectedFonts) {
			temp.push(fontData);
		}
		this.setState({
			selectedFonts: temp,
			showControls: true
		});
	},
	handleModalClose: function() {
		this.setState({
			showModal: false
		});
	},
	randomiseSelectedFonts: function() {
		var temp = [];
		var xRange = this.state.manifoldWidth - 1;
		var yRange = this.state.manifoldHeight - 1;

		var totalFonts = this.state.manifoldWidth * this.state.manifoldHeight;
		var amountOfFontsToReturn = Math.floor(totalFonts/5);
		if (totalFonts < amountOfFontsToReturn) {
			amountOfFontsToReturn = 1;
		} else if (amountOfFontsToReturn > this.state.maxSelectedFonts) {
			amountOfFontsToReturn = this.state.maxSelectedFonts;
		}

		for (var i = 0; i < amountOfFontsToReturn; i++) {
			var font = this.getRandomFont(xRange, yRange);
			while (this.isFontSelected(font, temp) > -1) { //while not unique
				font = this.getRandomFont(xRange, yRange);
			}
			temp[i] = font;
		} 
 		this.setState({
 			selectedFonts: temp
 		});
	},
	randomiseFont: function(selectedFontIndex) {
		var temp = this.state.selectedFonts.slice();
		var xRange = this.state.manifoldWidth - 1;
		var yRange = this.state.manifoldHeight - 1;
		var font = this.getRandomFont(xRange, yRange);
		while (this.isFontSelected(font, temp) > -1) { //while not unique
			font = this.getRandomFont(xRange, yRange);
		}
		temp[selectedFontIndex] = font;
		this.setState({
			selectedFonts : temp
		});
	},
	getRandomFont: function(xRange, yRange) {
		var randomX = helpers.randomInt(0, xRange);
		var randomY= helpers.randomInt(0, yRange);
		return [randomX, randomY];
	},
	toggleControls: function() {
		this.setState({
			showControls: !this.state.showControls
		});
	},
	downloadSelectedFonts: function() {
		if (this.state.selectedFonts.length > 0) {
			this.setState({
				showModal: true
			});
		}
	},
	render: function() {
		return (
			<div>
				<Manifold 
					manifoldData={this.state.manifoldData}
					maxSelectedFonts={this.state.maxSelectedFonts}
					selectedFonts={this.state.selectedFonts}
					updateSelectedFonts={this.updateSelectedFonts}
					toggleControls={this.toggleControls}
					serialiseFontCoords={this.serialiseFontCoords}
					serialiseMultiArray={this.serialiseMultiArray}
				/>
				<ControlPanel
					maxSelectedFonts={this.state.maxSelectedFonts}
					manifoldData={this.state.manifoldData}
					active={this.state.showControls}
					selectedFonts={this.state.selectedFonts}
					toggleControls={this.toggleControls}
					updateSelectedFonts={this.updateSelectedFonts}
					randomiseSelectedFonts={this.randomiseSelectedFonts}
					downloadSelectedFonts={this.downloadSelectedFonts}
					randomiseFont={this.randomiseFont}
				/>
				<Modal show={this.state.showModal} closeModal={this.handleModalClose}/>
			</div>
		);
	}
});
var Modal = createReactClass({
	handleModalBackgroundClick: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.props.closeModal();
	},
	render: function() {
		if (this.props.show) {
			return (<div>
						<div className={'modal'} onClick={this.handleModalBackgroundClick} onKeyDown={this.handleKeyDown}>
						</div>
						<DownloadModal closeModal={this.props.closeModal}/>
					</div>);
		} else {
			return null;
		}

	}
});
Modal.proptypes = {
	closeModal: PropTypes.func
}
var DownloadModal = createReactClass({
	getInitialState: function() {
		return {
			submitted: false,
			inputValue: ''
		}
	},
	handleInputChange: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.setState({
			inputValue: e.target.value
		});
	},
	handleModalClose: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.props.closeModal();
		this.setState({
			inputValue: ''
		});
	},
	handleKeyDown: function(e) {
		e.stopPropagation();
		if (e.keyCode == 27) {
			this.props.closeModal();
			this.setState({
				inputValue: ''
			});
		}
	},
	submitModal: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.setState({
			submitted: true
		});
	},
	render: function() {
		var modalContents;
		var submitClass = (this.state.inputValue.indexOf('@') > -1) ?  'active ' : '';
		if (!this.state.submitted) {
			modalContents = (<div className={'modal-contents'}>
								<p>Enter your email address to receive your fonts.</p>
								<input type='text' onChange={this.handleInputChange}></input>
								<button type='submit' className={submitClass} onClick={this.submitModal}>Send me my fonts!</button>
							</div>);
		} else {
			modalContents = (<div className={'modal-contents'}>
								<p>Your fonts will be with you shortly.</p>
							</div>);
		}
		return (<div className={'download-modal'} onKeyDown={this.handleKeyDown}>
					<i className={'material-icons'} onClick={this.handleModalClose}>close</i>
					{modalContents}
				</div>)
	}
});
Modal.proptypes = {
	closeModal: PropTypes.func
}


module.exports = {AppContainer: AppContainer};
