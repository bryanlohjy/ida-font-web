var React = require('react');
var ReactDOM = require('react-dom');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');

var ControlPanel = createReactClass({
	displayName: 'ControlPanel',
	handleCloseClick: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.props.toggleControls();
	},
	handleExpandClick: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.props.toggleControls();
	},
	handleDownloadClick: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.props.downloadSelectedFonts();
	},
	render: function() {
		var activeClass;
		if (this.props.selectedFonts.length > 0) {
			activeClass = 'active ';
		} else {
			activeClass = '';
		}
		var downloadButton = (<div className={'download ' + activeClass} onClick={this.handleDownloadClick}>
								<i className={'material-icons'}>file_download</i>
								Download
							</div>);
		if (this.props.active) {
			return (
				<div className={'control-panel'}>
					<div className={'control-panel-header'}>
						<div className={'logo'}>
							<div className={'logo-main'}>IDA</div>
							<div className={'logo-sub'}>[ font_labs ]</div>
						</div>
					</div>
					<div className={'close-controls'} onClick={this.handleCloseClick}>
						<i className={'material-icons'}>keyboard_arrow_right</i>
					</div>
					<div className={'controls'}>
						<SelectedFonts
							manifoldData={this.props.manifoldData}
							selectedFonts={this.props.selectedFonts}
							updateSelectedFonts={this.props.updateSelectedFonts}
							maxSelectedFonts={this.props.maxSelectedFonts}
							randomiseFont={this.props.randomiseFont}
						/>
						<div className={'actions'}>
							<div
								className={'random'}
								onClick={this.props.randomiseSelectedFonts}
							>
								<i className={'material-icons'}>refresh</i>
								Randomise All
							</div>
							{downloadButton}
						</div>
					</div>
				</div>
			);			
		} else {
			return (
				<div className={'expand-controls'} onClick={this.handleExpandClick}>
					<i className={'material-icons'}>keyboard_arrow_left</i>
				</div>
			);
		}
	}
});
ControlPanel.proptypes = {
	active: PropTypes.bool,
	selectedFonts: PropTypes.array,
	updateSelectedFonts: PropTypes.func,
	toggleControls: PropTypes.func,
	randomiseSelectedFonts: PropTypes.func,
	manifoldData: PropTypes.array,
	maxSelectedFonts: PropTypes.number,
	downloadSelectedFonts: PropTypes.func,
	randomiseFont: PropTypes.func
}

var SelectedFonts = createReactClass({
	displayName: 'SelectedFonts',
	render: function() {
		var fonts = [];
		var self = this;
		for (var i = 0; i < this.props.maxSelectedFonts; i++) {
			var fontCoords = [];
			var fontData = [];
			if (this.props.selectedFonts.length > 0 && this.props.manifoldData.length > 0) {
				fontCoords = this.props.selectedFonts[i];
				if (fontCoords && fontCoords.length > 0) {
					fontData = this.props.manifoldData[fontCoords[1]][fontCoords[0]];
				}
			}
			fonts.push(<SelectedFont
							key={i}
							fontIndex={fontCoords}
							fontData={fontData}
							controlIndex={i}
							updateSelectedFonts={self.props.updateSelectedFonts}
							randomiseFont={self.props.randomiseFont}
							selectedFonts={self.props.selectedFonts}
						/>);
		}
		return (<div className={'selected-font-container'}>{fonts}</div>);
	}
});
SelectedFonts.proptypes = {
	selectedFonts: PropTypes.array,
	updateSelectedFonts: PropTypes.func,
	manifoldData: PropTypes.array,
	maxSelectedFonts: PropTypes.number,
	randomiseFont: PropTypes.func
}

var SelectedFont = createReactClass({
	displayName: 'FontDisplay',
	handleClick: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.props.updateSelectedFonts(this.props.fontIndex);
	},
	handleRandomClick: function(e) {
		e.stopPropagation();
		e.preventDefault();
		this.props.randomiseFont(this.props.controlIndex);
	},
	render: function() {
		var activeClass = '';
		if (this.props.controlIndex <= this.props.selectedFonts.length || this.props.controlIndex == 0) {
			activeClass = 'active-font ';
			if (this.props.fontData.length == 0) {
				activeClass += 'empty ';
			}
		}
		return (
			<div className= {'selected-font ' + activeClass}>
				<div className= {'delete-font'}>
					<i className={'material-icons'} onClick={this.handleClick}>remove</i>
				</div>
				<div className= {'randomise-font'}>
					<i className={'material-icons'} onClick={this.handleRandomClick}>refresh</i>
				</div>
				<svg  className={'font-glyph'}>
					<use 
						href= {'#'+ this.props.fontData}
						className = {'fontDataSvg'}
					/>
				</svg>
			</div>
		)
	}
});
SelectedFont.proptypes = {
	controlIndex: PropTypes.number,
	fontData: PropTypes.number,
	updateSelectedFonts: PropTypes.func,
	fontIndex: PropTypes.array,
	randomiseFont: PropTypes.func,
	selectedFonts: PropTypes.array
}

module.exports = {ControlPanel: ControlPanel};